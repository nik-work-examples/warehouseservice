package WHS.warehouseservice.model;

public enum Color {
    GREEN("зеленый"),
    RED("красный"),
    YELLOW("желтый");
	
	private String description;
	
	private Color(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
}
