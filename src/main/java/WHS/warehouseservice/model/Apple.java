package WHS.warehouseservice.model;

public class Apple {
		Long id;
        Double weight;
        Color color;
        String sort;
        Boolean sour;

        public Double getWeight() {
            return weight;
        }
        public Color getColor() {
            return color;
        }
        public String getSort() {
            return sort;
        }
        public Long getId() {
            return id;
        }
        public Boolean getSour() {
            return sour;
        }

        public void setWeight(Double weight) {
            this.weight = weight;
        }
        public void setColor(Color color) {
            this.color = color;
        }
        public void setSort(String sort) {
            this.sort = sort;
        }
        public void setId(Long id) {
            this.id = id;
        }
        public void setSour(Boolean sour) {
            this.sour = sour;
        }
    }
    