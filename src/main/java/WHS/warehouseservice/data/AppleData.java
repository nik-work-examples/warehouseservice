package WHS.warehouseservice.data;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import WHS.warehouseservice.model.Apple;

@Component
public class AppleData {
	
    private Map<Long, Apple> apples = new HashMap<>();
    
    private Long currentIndex = 0L;

    public Map<Long, Apple> getApples() {
        return apples;
    }
    
    public Long nextIndex() {
    	return currentIndex++;
    }
}
