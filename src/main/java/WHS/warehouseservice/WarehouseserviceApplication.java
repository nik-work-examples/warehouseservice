package WHS.warehouseservice;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import WHS.warehouseservice.data.AppleData;

@SpringBootApplication
public class WarehouseserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarehouseserviceApplication.class, args);
	}
	
	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
	public AppleData singletonAppleData() {
		return new AppleData();
	}
	
}
