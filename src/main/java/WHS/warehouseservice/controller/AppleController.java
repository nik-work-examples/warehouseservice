package WHS.warehouseservice.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import WHS.warehouseservice.model.Apple;
import WHS.warehouseservice.service.AppleService;

@RestController
@RequestMapping("/apple")
public class AppleController {
	
	@Autowired
	private AppleService appleService;
	
    @GetMapping("/info/{id}")
    public Apple info(@PathVariable Long id){
        return appleService.info(id);
    }

    @PostMapping("/add")
    public Apple add(@RequestBody Apple apple) throws Exception {
    	return appleService.add(apple);
    }
    
    @PostMapping("/change")
    public Apple change(@RequestBody Apple apple) throws Exception{
    	return appleService.change(apple);
    }
    
    @PostMapping("/remove")
    public Apple remove(Long id){
    	return appleService.remove(id);
    }
    
    @GetMapping("/all") 
    public Collection<Apple> getAll() {
    	return appleService.getAll();
    }
    
}
