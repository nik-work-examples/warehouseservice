package WHS.warehouseservice.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AppleControllerAdvice {

	@ExceptionHandler({Exception.class})
	public String handle(Exception e) {
		return e.getMessage();
	}
	
}
