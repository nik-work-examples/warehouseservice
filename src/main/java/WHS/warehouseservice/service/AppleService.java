package WHS.warehouseservice.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import WHS.warehouseservice.data.AppleData;
import WHS.warehouseservice.model.Apple;

@Service
public class AppleService {
	
	@Autowired
	@Qualifier(value = "singletonAppleData")
	private AppleData singletonAppleData;
	
    public Apple info(Long id) {
    	return singletonAppleData.getApples().get(id);
    }
    
    public Apple add(Apple apple) throws Exception {
    	validate(apple);
    	Long id = singletonAppleData.nextIndex();
    	apple.setId(id);
    	singletonAppleData.getApples().put(id, apple);
    	return apple;
    }
    
    public Apple change(Apple apple) throws Exception{
    	validate(apple);
    	singletonAppleData.getApples().put(apple.getId(), apple);
    	return apple;
    }
    
    public Apple remove(Long id){
    	Apple apple = singletonAppleData.getApples().remove(id);
    	return apple;
    }
    
    private void validate(Apple apple) throws Exception {
    	StringBuilder sb = new StringBuilder();
    	Double weight = apple.getWeight();
    	if(weight == null || weight.compareTo(Double.valueOf(0)) <= 0) {
    		sb.append("Указан некорректный вес: ").append(weight).append('\n');
    	}
    	if(apple.getColor() == null) {
    		sb.append("Указан некорректный цвет: ").append(apple.getColor()).append('\n');
    	}
    	String sort = apple.getSort();
    	if(sort == null) {
    		sb.append("Не указан сорт.").append('\n');
    	}
    	if(sb.length() > 0) {
    		throw new Exception(sb.toString());
    	}
    }

	public Collection<Apple> getAll() {
		return singletonAppleData.getApples().values();
	}
    
}
